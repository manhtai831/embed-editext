package com.example.embed_editext;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.flutter.plugin.platform.PlatformView;
import java.util.Map;

public class EditTextAndroidView implements PlatformView {
    @NonNull private final EditText textView;

    EditTextAndroidView(@NonNull Context context, int id, @Nullable Map<String, Object> creationParams) {
        textView = new EditText(context);
        textView.setTextSize(16);
        textView.setText("Rendered on a native Android view (id: " + id + ")");
    }

    @NonNull
    @Override
    public View getView() {
        return textView;
    }

    @Override
    public void dispose() {}
}
